-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: inmuebles
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alquilado`
--

DROP TABLE IF EXISTS `alquilado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alquilado` (
  `idalquilado` bigint(20) NOT NULL AUTO_INCREMENT,
  `alquilado` varchar(100) NOT NULL,
  `fkidestado` bigint(20) NOT NULL,
  `fkidinquilono` bigint(20) NOT NULL,
  `fkidestado_alquilado` bigint(20) NOT NULL,
  `fkidtipo_alquilado` bigint(20) NOT NULL,
  PRIMARY KEY (`idalquilado`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidinquilono` (`fkidinquilono`),
  KEY `fkidestado_alquilado` (`fkidestado_alquilado`),
  CONSTRAINT `alquilado_ibfk_3` FOREIGN KEY (`fkidestado_alquilado`) REFERENCES `estado_alquilado` (`idestado_alquilado`),
  CONSTRAINT `alquilado_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `alquilado_ibfk_2` FOREIGN KEY (`fkidinquilono`) REFERENCES `inquilino` (`idinquilino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alquilado`
--

LOCK TABLES `alquilado` WRITE;
/*!40000 ALTER TABLE `alquilado` DISABLE KEYS */;
/*!40000 ALTER TABLE `alquilado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_cargo` varchar(50) NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `idcontrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `contrato` varchar(100) NOT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  `fkidestado_contrato` bigint(20) DEFAULT NULL,
  `fkidfecha_creacion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcontrato`),
  KEY `contrato` (`fkidestado`),
  KEY `fkidestado_contrato` (`fkidestado_contrato`),
  KEY `fkidfecha_creacion` (`fkidfecha_creacion`),
  CONSTRAINT `contrato_ibfk_3` FOREIGN KEY (`fkidfecha_creacion`) REFERENCES `fecha_creacion` (`idfecha_creacion`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`fkidestado_contrato`) REFERENCES `estado_contrato` (`idestado_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idempleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `edad` bigint(20) NOT NULL,
  `fkidcargo` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  `fkidcontrato` bigint(20) NOT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `fkidcontrato` (`fkidcontrato`),
  KEY `fkidcargo` (`fkidcargo`),
  KEY `fkidestado` (`fkidestado`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidcontrato`) REFERENCES `contrato` (`idcontrato`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkidcargo`) REFERENCES `cargo` (`idcargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `ideps` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  PRIMARY KEY (`ideps`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idestado` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_estado` varchar(30) NOT NULL,
  `fkidgerente` bigint(20) NOT NULL,
  PRIMARY KEY (`idestado`),
  KEY `fkidgerente` (`fkidgerente`),
  CONSTRAINT `estado_ibfk_1` FOREIGN KEY (`fkidgerente`) REFERENCES `gerente` (`idgerente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_alquilado`
--

DROP TABLE IF EXISTS `estado_alquilado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_alquilado` (
  `idestado_alquilado` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_alquilado` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_alquilado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_alquilado`
--

LOCK TABLES `estado_alquilado` WRITE;
/*!40000 ALTER TABLE `estado_alquilado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_alquilado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_civil`
--

DROP TABLE IF EXISTS `estado_civil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_civil` (
  `idestado_civil` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_civil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_civil`
--

LOCK TABLES `estado_civil` WRITE;
/*!40000 ALTER TABLE `estado_civil` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_civil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_contrato`
--

DROP TABLE IF EXISTS `estado_contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_contrato` (
  `idestado_contrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_contrato` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_contrato`
--

LOCK TABLES `estado_contrato` WRITE;
/*!40000 ALTER TABLE `estado_contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_inmueble`
--

DROP TABLE IF EXISTS `estado_inmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_inmueble` (
  `idestado_inmueble` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_inmueble` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_inmueble`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_inmueble`
--

LOCK TABLES `estado_inmueble` WRITE;
/*!40000 ALTER TABLE `estado_inmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_inmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_inquilino`
--

DROP TABLE IF EXISTS `estado_inquilino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_inquilino` (
  `idestado_inquilino` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_inquiino` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_inquilino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_inquilino`
--

LOCK TABLES `estado_inquilino` WRITE;
/*!40000 ALTER TABLE `estado_inquilino` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_inquilino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_piso`
--

DROP TABLE IF EXISTS `estado_piso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_piso` (
  `idestado_piso` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_piso` varchar(50) NOT NULL,
  PRIMARY KEY (`idestado_piso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_piso`
--

LOCK TABLES `estado_piso` WRITE;
/*!40000 ALTER TABLE `estado_piso` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_piso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fecha_creacion`
--

DROP TABLE IF EXISTS `fecha_creacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha_creacion` (
  `idfecha_creacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` varchar(100) NOT NULL,
  PRIMARY KEY (`idfecha_creacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha_creacion`
--

LOCK TABLES `fecha_creacion` WRITE;
/*!40000 ALTER TABLE `fecha_creacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `fecha_creacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idgenero` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_genero` varchar(50) NOT NULL,
  PRIMARY KEY (`idgenero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gerente`
--

DROP TABLE IF EXISTS `gerente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gerente` (
  `idgerente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  PRIMARY KEY (`idgerente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gerente`
--

LOCK TABLES `gerente` WRITE;
/*!40000 ALTER TABLE `gerente` DISABLE KEYS */;
/*!40000 ALTER TABLE `gerente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inmueble`
--

DROP TABLE IF EXISTS `inmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inmueble` (
  `idinmueble` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `localizacion` varchar(100) NOT NULL,
  `idestado_inmueble` bigint(20) DEFAULT NULL,
  `idtipo_inmueble` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) NOT NULL,
  PRIMARY KEY (`idinmueble`),
  KEY `idestado_inmueble` (`idestado_inmueble`),
  KEY `idtipo_inmueble` (`idtipo_inmueble`),
  KEY `fkidestado` (`fkidestado`),
  CONSTRAINT `inmueble_ibfk_3` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `inmueble_ibfk_1` FOREIGN KEY (`idestado_inmueble`) REFERENCES `estado_inmueble` (`idestado_inmueble`),
  CONSTRAINT `inmueble_ibfk_2` FOREIGN KEY (`idtipo_inmueble`) REFERENCES `tipo_inmueble` (`idtipo_inmueble`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inmueble`
--

LOCK TABLES `inmueble` WRITE;
/*!40000 ALTER TABLE `inmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inquilino`
--

DROP TABLE IF EXISTS `inquilino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inquilino` (
  `idinquilino` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `edad` bigint(20) NOT NULL,
  `fkidestado_inquilino` bigint(20) DEFAULT NULL,
  `fkidestado_civil` bigint(20) DEFAULT NULL,
  `fkideps` bigint(20) DEFAULT NULL,
  `fkidgenero` bigint(20) DEFAULT NULL,
  `fktipo_documento` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idinquilino`),
  KEY `inquilino` (`fkideps`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidestado_inquilino` (`fkidestado_inquilino`),
  KEY `fkidgenero` (`fkidgenero`),
  KEY `fkidestado_civil` (`fkidestado_civil`),
  KEY `fktipo_documento` (`fktipo_documento`),
  CONSTRAINT `inquilino_ibfk_6` FOREIGN KEY (`fktipo_documento`) REFERENCES `tipo_documento` (`idtipo_documento`),
  CONSTRAINT `inquilino_ibfk_1` FOREIGN KEY (`fkideps`) REFERENCES `eps` (`ideps`),
  CONSTRAINT `inquilino_ibfk_2` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `inquilino_ibfk_3` FOREIGN KEY (`fkidestado_inquilino`) REFERENCES `estado_inquilino` (`idestado_inquilino`),
  CONSTRAINT `inquilino_ibfk_4` FOREIGN KEY (`fkidgenero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `inquilino_ibfk_5` FOREIGN KEY (`fkidestado_civil`) REFERENCES `estado_civil` (`idestado_civil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inquilino`
--

LOCK TABLES `inquilino` WRITE;
/*!40000 ALTER TABLE `inquilino` DISABLE KEYS */;
/*!40000 ALTER TABLE `inquilino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piso`
--

DROP TABLE IF EXISTS `piso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piso` (
  `idpiso` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `numero_piso` bigint(20) NOT NULL,
  `fkidestado` bigint(20) NOT NULL,
  `fkidestado_piso` bigint(20) NOT NULL,
  `fkidtipo_piso` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idpiso`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidestado_piso` (`fkidestado_piso`),
  KEY `fkidtipo_piso` (`fkidtipo_piso`),
  CONSTRAINT `piso_ibfk_3` FOREIGN KEY (`fkidtipo_piso`) REFERENCES `tipo_piso` (`idtipo_piso`),
  CONSTRAINT `piso_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `piso_ibfk_2` FOREIGN KEY (`fkidestado_piso`) REFERENCES `estado_piso` (`idestado_piso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piso`
--

LOCK TABLES `piso` WRITE;
/*!40000 ALTER TABLE `piso` DISABLE KEYS */;
/*!40000 ALTER TABLE `piso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_alquilado`
--

DROP TABLE IF EXISTS `tipo_alquilado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_alquilado` (
  `idtipo_alquilado` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_aquilado` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo_alquilado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_alquilado`
--

LOCK TABLES `tipo_alquilado` WRITE;
/*!40000 ALTER TABLE `tipo_alquilado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_alquilado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `idtipo_documento` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_documento` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo_documento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_inmueble`
--

DROP TABLE IF EXISTS `tipo_inmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_inmueble` (
  `idtipo_inmueble` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_inmueble` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo_inmueble`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_inmueble`
--

LOCK TABLES `tipo_inmueble` WRITE;
/*!40000 ALTER TABLE `tipo_inmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_inmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_piso`
--

DROP TABLE IF EXISTS `tipo_piso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_piso` (
  `idtipo_piso` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_piso` bigint(20) NOT NULL,
  PRIMARY KEY (`idtipo_piso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_piso`
--

LOCK TABLES `tipo_piso` WRITE;
/*!40000 ALTER TABLE `tipo_piso` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_piso` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-20  0:17:08
