-- MySQL dump 10.13  Distrib 5.5.24, for Win64 (x86)
--
-- Host: localhost    Database: maderas
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_cargo` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `fkidcargo_empleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idcargo`),
  KEY `cargo` (`fkidcargo_empleado`),
  CONSTRAINT `cargo_ibfk_1` FOREIGN KEY (`fkidcargo_empleado`) REFERENCES `cargo_empleado` (`idcargo_empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo_empleado`
--

DROP TABLE IF EXISTS `cargo_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo_empleado` (
  `idcargo_empleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `edad` bigint(20) NOT NULL,
  `genero` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `nombre_cargo` varchar(50) NOT NULL,
  `area` varchar(50) NOT NULL,
  `fecha_asignacion` varchar(50) NOT NULL,
  PRIMARY KEY (`idcargo_empleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo_empleado`
--

LOCK TABLES `cargo_empleado` WRITE;
/*!40000 ALTER TABLE `cargo_empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo_empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idcliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(100) NOT NULL,
  `genero` varchar(50) NOT NULL,
  `edad` bigint(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `idcontrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_contrato` varchar(100) DEFAULT NULL,
  `fecha_inicio` varchar(100) NOT NULL,
  `fecha_entrega` varchar(100) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `fkidcliente` bigint(20) NOT NULL,
  `fkiddetalle_contrato` bigint(20) NOT NULL,
  PRIMARY KEY (`idcontrato`),
  KEY `contrato` (`fkidcliente`),
  KEY `fkiddetalle_contrato` (`fkiddetalle_contrato`),
  CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`fkiddetalle_contrato`) REFERENCES `detalle_contrato` (`iddetalle_contrato`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`fkidcliente`) REFERENCES `cliente` (`idcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_contrato`
--

DROP TABLE IF EXISTS `detalle_contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_contrato` (
  `iddetalle_contrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_empleado` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `fecha_inicio` varchar(100) NOT NULL,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`iddetalle_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_contrato`
--

LOCK TABLES `detalle_contrato` WRITE;
/*!40000 ALTER TABLE `detalle_contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diseño`
--

DROP TABLE IF EXISTS `diseño`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diseño` (
  `iddiseño` bigint(20) NOT NULL AUTO_INCREMENT,
  `color` varchar(50) NOT NULL,
  `tamaño` varchar(50) NOT NULL,
  `forma` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`iddiseño`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diseño`
--

LOCK TABLES `diseño` WRITE;
/*!40000 ALTER TABLE `diseño` DISABLE KEYS */;
/*!40000 ALTER TABLE `diseño` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idempleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_empleado` varchar(100) NOT NULL,
  `edad` bigint(20) NOT NULL,
  `genero` varchar(20) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `fkidcargo_empleado` bigint(20) NOT NULL,
  `fkiddetalle_contrato` bigint(20) NOT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `empleado` (`fkidcargo_empleado`),
  KEY `fkiddetalle_contrato` (`fkiddetalle_contrato`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidcargo_empleado`) REFERENCES `cargo_empleado` (`idcargo_empleado`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkiddetalle_contrato`) REFERENCES `detalle_contrato` (`iddetalle_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_compra`
--

DROP TABLE IF EXISTS `factura_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura_compra` (
  `idfactura_compra` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechafactura_compra` varchar(100) NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `fkidempleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idfactura_compra`),
  KEY `factura_compra` (`fkidempleado`),
  CONSTRAINT `factura_compra_ibfk_1` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_compra`
--

LOCK TABLES `factura_compra` WRITE;
/*!40000 ALTER TABLE `factura_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura_venta`
--

DROP TABLE IF EXISTS `factura_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura_venta` (
  `idfeactura_venta` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechafactura_venta` varchar(100) NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `garantia` varchar(100) NOT NULL,
  `fkidcliente` bigint(20) NOT NULL,
  PRIMARY KEY (`idfeactura_venta`),
  KEY `fkidcliente` (`fkidcliente`),
  CONSTRAINT `factura_venta_ibfk_1` FOREIGN KEY (`fkidcliente`) REFERENCES `cliente` (`idcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura_venta`
--

LOCK TABLES `factura_venta` WRITE;
/*!40000 ALTER TABLE `factura_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia_prima`
--

DROP TABLE IF EXISTS `materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia_prima` (
  `idmateria_prima` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_materiaPrima` varchar(100) NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `tamaño` varchar(20) NOT NULL,
  `fkiddiseño` bigint(20) NOT NULL,
  `fkidfactura_compra` bigint(20) NOT NULL,
  PRIMARY KEY (`idmateria_prima`),
  KEY `materia_prima` (`fkiddiseño`),
  KEY `fkidfactura_compra` (`fkidfactura_compra`),
  CONSTRAINT `materia_prima_ibfk_2` FOREIGN KEY (`fkidfactura_compra`) REFERENCES `factura_compra` (`idfactura_compra`),
  CONSTRAINT `materia_prima_ibfk_1` FOREIGN KEY (`fkiddiseño`) REFERENCES `diseño` (`iddiseño`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia_prima`
--

LOCK TABLES `materia_prima` WRITE;
/*!40000 ALTER TABLE `materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `idproducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(100) NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `tamaño` varchar(50) DEFAULT NULL,
  `fkidfactura_venta` bigint(20) NOT NULL,
  `fkidcontrato` bigint(20) NOT NULL,
  PRIMARY KEY (`idproducto`),
  KEY `producto` (`fkidfactura_venta`),
  KEY `fkidcontrato` (`fkidcontrato`),
  CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`fkidcontrato`) REFERENCES `contrato` (`idcontrato`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`fkidfactura_venta`) REFERENCES `factura_venta` (`idfeactura_venta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 23:30:50
