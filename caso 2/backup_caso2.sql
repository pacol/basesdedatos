-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: lol
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividades_realizadas`
--

DROP TABLE IF EXISTS `actividades_realizadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actividades_realizadas` (
  `idactividades_realizadas` bigint(20) NOT NULL AUTO_INCREMENT,
  `actividades_realizadas` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idactividades_realizadas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividades_realizadas`
--

LOCK TABLES `actividades_realizadas` WRITE;
/*!40000 ALTER TABLE `actividades_realizadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividades_realizadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cable`
--

DROP TABLE IF EXISTS `cable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable` (
  `idcable` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `longitud` varchar(50) DEFAULT NULL,
  `fkidtipo_cable` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcable`),
  KEY `tipo_cablecable` (`fkidtipo_cable`),
  CONSTRAINT `cable_ibfk_1` FOREIGN KEY (`fkidtipo_cable`) REFERENCES `tipo_cable` (`idtipo_cable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cable`
--

LOCK TABLES `cable` WRITE;
/*!40000 ALTER TABLE `cable` DISABLE KEYS */;
/*!40000 ALTER TABLE `cable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_cargo` varchar(50) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcargo`),
  KEY `estadocargo` (`fkidestado`),
  CONSTRAINT `cargo_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronograma`
--

DROP TABLE IF EXISTS `cronograma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronograma` (
  `idcronograma` bigint(20) NOT NULL AUTO_INCREMENT,
  `cronograma` varchar(100) DEFAULT NULL,
  `fkidhoras_de_trabajo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcronograma`),
  KEY `horas_de_trabajocronograma` (`fkidhoras_de_trabajo`),
  CONSTRAINT `cronograma_ibfk_1` FOREIGN KEY (`fkidhoras_de_trabajo`) REFERENCES `horas_de_trabajo` (`idhoras_de_trabajo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronograma`
--

LOCK TABLES `cronograma` WRITE;
/*!40000 ALTER TABLE `cronograma` DISABLE KEYS */;
/*!40000 ALTER TABLE `cronograma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dato`
--

DROP TABLE IF EXISTS `dato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dato` (
  `iddato` bigint(20) NOT NULL AUTO_INCREMENT,
  `dato` varchar(100) DEFAULT NULL,
  `fkidtipo_dato` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`iddato`),
  KEY `tipo_datodato` (`fkidtipo_dato`),
  CONSTRAINT `dato_ibfk_1` FOREIGN KEY (`fkidtipo_dato`) REFERENCES `tipo_dato` (`idtipo_dato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dato`
--

LOCK TABLES `dato` WRITE;
/*!40000 ALTER TABLE `dato` DISABLE KEYS */;
/*!40000 ALTER TABLE `dato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia`
--

DROP TABLE IF EXISTS `dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia` (
  `iddia` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_dia` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`iddia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia`
--

LOCK TABLES `dia` WRITE;
/*!40000 ALTER TABLE `dia` DISABLE KEYS */;
/*!40000 ALTER TABLE `dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia_laborado`
--

DROP TABLE IF EXISTS `dia_laborado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia_laborado` (
  `iddia_laborado` bigint(20) NOT NULL AUTO_INCREMENT,
  `dia_laborado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`iddia_laborado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_laborado`
--

LOCK TABLES `dia_laborado` WRITE;
/*!40000 ALTER TABLE `dia_laborado` DISABLE KEYS */;
/*!40000 ALTER TABLE `dia_laborado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idempleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `fkidcargo` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `cargoempleado` (`fkidcargo`),
  KEY `estadoempleado` (`fkidestado`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidcargo`) REFERENCES `cargo` (`idcargo`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `idempresa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `fkidproveedor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idempresa`),
  KEY `proveedorempresa` (`fkidproveedor`),
  CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`fkidproveedor`) REFERENCES `proveedor` (`idproveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa_cliente`
--

DROP TABLE IF EXISTS `empresa_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa_cliente` (
  `idempresa_cliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `fkidubicacion` bigint(20) DEFAULT NULL,
  `fkidtipo_empresa_cliente` bigint(20) DEFAULT NULL,
  `fkidred` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idempresa_cliente`),
  KEY `ubicacionempresa_cliente` (`fkidubicacion`),
  KEY `tipo_empresa_clienteempresa_cliente` (`fkidtipo_empresa_cliente`),
  KEY `redempresa_cliente` (`fkidred`),
  CONSTRAINT `empresa_cliente_ibfk_1` FOREIGN KEY (`fkidubicacion`) REFERENCES `ubicacion` (`idubicacion`),
  CONSTRAINT `empresa_cliente_ibfk_2` FOREIGN KEY (`fkidtipo_empresa_cliente`) REFERENCES `tipo_empresa_cliente` (`idtipo_empresa_cliente`),
  CONSTRAINT `empresa_cliente_ibfk_3` FOREIGN KEY (`fkidred`) REFERENCES `red` (`idred`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa_cliente`
--

LOCK TABLES `empresa_cliente` WRITE;
/*!40000 ALTER TABLE `empresa_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idestado` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_estado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_materia_prima`
--

DROP TABLE IF EXISTS `estado_materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_materia_prima` (
  `idestado_materia_prima` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_materia_prima` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idestado_materia_prima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_materia_prima`
--

LOCK TABLES `estado_materia_prima` WRITE;
/*!40000 ALTER TABLE `estado_materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_proyecto`
--

DROP TABLE IF EXISTS `estado_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_proyecto` (
  `idestado_proyecto` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_proyecto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idestado_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_proyecto`
--

LOCK TABLES `estado_proyecto` WRITE;
/*!40000 ALTER TABLE `estado_proyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoja_de_vida`
--

DROP TABLE IF EXISTS `hoja_de_vida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hoja_de_vida` (
  `idhoja_de_vida` bigint(20) NOT NULL AUTO_INCREMENT,
  `hoja_de_vida` varchar(100) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idhoja_de_vida`),
  KEY `estadohoja_de_vida` (`fkidestado`),
  CONSTRAINT `hoja_de_vida_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoja_de_vida`
--

LOCK TABLES `hoja_de_vida` WRITE;
/*!40000 ALTER TABLE `hoja_de_vida` DISABLE KEYS */;
/*!40000 ALTER TABLE `hoja_de_vida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horas_de_trabajo`
--

DROP TABLE IF EXISTS `horas_de_trabajo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horas_de_trabajo` (
  `idhoras_de_trabajo` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_horas_de_trabajo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idhoras_de_trabajo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horas_de_trabajo`
--

LOCK TABLES `horas_de_trabajo` WRITE;
/*!40000 ALTER TABLE `horas_de_trabajo` DISABLE KEYS */;
/*!40000 ALTER TABLE `horas_de_trabajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `implantacion`
--

DROP TABLE IF EXISTS `implantacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `implantacion` (
  `idimplantacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_implantacion` varchar(50) DEFAULT NULL,
  `fkiddato` bigint(20) DEFAULT NULL,
  `fkidcable` bigint(20) DEFAULT NULL,
  `fkidprotocolo` bigint(20) DEFAULT NULL,
  `fkidtipologia` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idimplantacion`),
  KEY `datoimplantacion` (`fkiddato`),
  KEY `cableimplantacion` (`fkidcable`),
  KEY `protocoloimplantacion` (`fkidprotocolo`),
  KEY `tipologiaimplantacion` (`fkidtipologia`),
  CONSTRAINT `implantacion_ibfk_1` FOREIGN KEY (`fkiddato`) REFERENCES `dato` (`iddato`),
  CONSTRAINT `implantacion_ibfk_2` FOREIGN KEY (`fkidcable`) REFERENCES `cable` (`idcable`),
  CONSTRAINT `implantacion_ibfk_3` FOREIGN KEY (`fkidprotocolo`) REFERENCES `protocolo` (`idprotocolo`),
  CONSTRAINT `implantacion_ibfk_4` FOREIGN KEY (`fkidtipologia`) REFERENCES `tipologia` (`idtipologia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `implantacion`
--

LOCK TABLES `implantacion` WRITE;
/*!40000 ALTER TABLE `implantacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `implantacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario_materia_prima`
--

DROP TABLE IF EXISTS `inventario_materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario_materia_prima` (
  `idinventario_materia_prima` bigint(20) NOT NULL AUTO_INCREMENT,
  `inventario_materia_prima` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idinventario_materia_prima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario_materia_prima`
--

LOCK TABLES `inventario_materia_prima` WRITE;
/*!40000 ALTER TABLE `inventario_materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventario_materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia_prima`
--

DROP TABLE IF EXISTS `materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia_prima` (
  `idmateria_prima` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `fkidestado_materia_prima` bigint(20) DEFAULT NULL,
  `fkidinventario_materia_prima` bigint(20) DEFAULT NULL,
  `fkidtipo_materia_prima` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idmateria_prima`),
  KEY `estado_materia_primamateria_prima` (`fkidestado_materia_prima`),
  KEY `inventario_materia_primamateria_prima` (`fkidinventario_materia_prima`),
  KEY `tipo_materia_primamateria_prima` (`fkidtipo_materia_prima`),
  CONSTRAINT `materia_prima_ibfk_1` FOREIGN KEY (`fkidestado_materia_prima`) REFERENCES `estado_materia_prima` (`idestado_materia_prima`),
  CONSTRAINT `materia_prima_ibfk_2` FOREIGN KEY (`fkidinventario_materia_prima`) REFERENCES `inventario_materia_prima` (`idinventario_materia_prima`),
  CONSTRAINT `materia_prima_ibfk_3` FOREIGN KEY (`fkidtipo_materia_prima`) REFERENCES `tipo_materia_prima` (`idtipo_materia_prima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia_prima`
--

LOCK TABLES `materia_prima` WRITE;
/*!40000 ALTER TABLE `materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_involucrada`
--

DROP TABLE IF EXISTS `persona_involucrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_involucrada` (
  `idpersona_involucrada` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `fkidhoja_de_vida` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idpersona_involucrada`),
  KEY `hoja_de_vidapersona_involucrada` (`fkidhoja_de_vida`),
  CONSTRAINT `persona_involucrada_ibfk_1` FOREIGN KEY (`fkidhoja_de_vida`) REFERENCES `hoja_de_vida` (`idhoja_de_vida`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_involucrada`
--

LOCK TABLES `persona_involucrada` WRITE;
/*!40000 ALTER TABLE `persona_involucrada` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona_involucrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presupuesto_dia`
--

DROP TABLE IF EXISTS `presupuesto_dia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presupuesto_dia` (
  `idpresupuesto_dia` bigint(20) NOT NULL AUTO_INCREMENT,
  `presupuesto_dia` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idpresupuesto_dia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presupuesto_dia`
--

LOCK TABLES `presupuesto_dia` WRITE;
/*!40000 ALTER TABLE `presupuesto_dia` DISABLE KEYS */;
/*!40000 ALTER TABLE `presupuesto_dia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problemas_presupuestados`
--

DROP TABLE IF EXISTS `problemas_presupuestados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemas_presupuestados` (
  `idproblemas_presupuestados` bigint(20) NOT NULL AUTO_INCREMENT,
  `problemas_presupuestados` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idproblemas_presupuestados`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problemas_presupuestados`
--

LOCK TABLES `problemas_presupuestados` WRITE;
/*!40000 ALTER TABLE `problemas_presupuestados` DISABLE KEYS */;
/*!40000 ALTER TABLE `problemas_presupuestados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problemas_reales`
--

DROP TABLE IF EXISTS `problemas_reales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problemas_reales` (
  `idproblemas_reales` bigint(20) NOT NULL AUTO_INCREMENT,
  `problemas_reales` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idproblemas_reales`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problemas_reales`
--

LOCK TABLES `problemas_reales` WRITE;
/*!40000 ALTER TABLE `problemas_reales` DISABLE KEYS */;
/*!40000 ALTER TABLE `problemas_reales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `protocolo`
--

DROP TABLE IF EXISTS `protocolo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `protocolo` (
  `idprotocolo` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `fkidtipo_protocolo` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idprotocolo`),
  KEY `tipo_protocoloprotocolo` (`fkidtipo_protocolo`),
  CONSTRAINT `protocolo_ibfk_1` FOREIGN KEY (`fkidtipo_protocolo`) REFERENCES `tipo_protocolo` (`idtipo_protocolo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `protocolo`
--

LOCK TABLES `protocolo` WRITE;
/*!40000 ALTER TABLE `protocolo` DISABLE KEYS */;
/*!40000 ALTER TABLE `protocolo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `idproveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `nit` bigint(20) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idproveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `idproyecto` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `fkidpersona_involucrada` bigint(20) DEFAULT NULL,
  `fkidempresa_cliente` bigint(20) DEFAULT NULL,
  `fkidimplantacion` bigint(20) DEFAULT NULL,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkiddia_laborado` bigint(20) DEFAULT NULL,
  `fkidpresupuesto_dia` bigint(20) DEFAULT NULL,
  `fkiddia` bigint(20) DEFAULT NULL,
  `fkidactividades_realizadas` bigint(20) DEFAULT NULL,
  `fkidproblemas_presupuestados` bigint(20) DEFAULT NULL,
  `fkidproblemas_reales` bigint(20) DEFAULT NULL,
  `fkidcronograma` bigint(20) DEFAULT NULL,
  `fkidestado_proyecto` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idproyecto`),
  KEY `persona_involucradaproyecto` (`fkidpersona_involucrada`),
  KEY `empresa_clienteproyecto` (`fkidempresa_cliente`),
  KEY `implantacionproyecto` (`fkidimplantacion`),
  KEY `empleadoproyecto` (`fkidempleado`),
  KEY `dia_laboradoproyecto` (`fkiddia_laborado`),
  KEY `presupuesto_diaproyecto` (`fkidpresupuesto_dia`),
  KEY `diaproyecto` (`fkiddia`),
  KEY `actividades_realizadasproyecto` (`fkidactividades_realizadas`),
  KEY `problemas_presupuestadosproyecto` (`fkidproblemas_presupuestados`),
  KEY `problemas_realesproyecto` (`fkidproblemas_reales`),
  KEY `cronogramaproyecto` (`fkidcronograma`),
  KEY `estado_proyectoproyecto` (`fkidestado_proyecto`),
  KEY `estadoproyecto` (`fkidestado`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`fkidpersona_involucrada`) REFERENCES `persona_involucrada` (`idpersona_involucrada`),
  CONSTRAINT `proyecto_ibfk_2` FOREIGN KEY (`fkidempresa_cliente`) REFERENCES `empresa_cliente` (`idempresa_cliente`),
  CONSTRAINT `proyecto_ibfk_3` FOREIGN KEY (`fkidimplantacion`) REFERENCES `implantacion` (`idimplantacion`),
  CONSTRAINT `proyecto_ibfk_4` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`),
  CONSTRAINT `proyecto_ibfk_5` FOREIGN KEY (`fkiddia_laborado`) REFERENCES `dia_laborado` (`iddia_laborado`),
  CONSTRAINT `proyecto_ibfk_6` FOREIGN KEY (`fkidpresupuesto_dia`) REFERENCES `presupuesto_dia` (`idpresupuesto_dia`),
  CONSTRAINT `proyecto_ibfk_7` FOREIGN KEY (`fkiddia`) REFERENCES `dia` (`iddia`),
  CONSTRAINT `proyecto_ibfk_8` FOREIGN KEY (`fkidactividades_realizadas`) REFERENCES `actividades_realizadas` (`idactividades_realizadas`),
  CONSTRAINT `proyecto_ibfk_9` FOREIGN KEY (`fkidproblemas_presupuestados`) REFERENCES `problemas_presupuestados` (`idproblemas_presupuestados`),
  CONSTRAINT `proyecto_ibfk_10` FOREIGN KEY (`fkidproblemas_reales`) REFERENCES `problemas_reales` (`idproblemas_reales`),
  CONSTRAINT `proyecto_ibfk_11` FOREIGN KEY (`fkidcronograma`) REFERENCES `cronograma` (`idcronograma`),
  CONSTRAINT `proyecto_ibfk_12` FOREIGN KEY (`fkidestado_proyecto`) REFERENCES `estado_proyecto` (`idestado_proyecto`),
  CONSTRAINT `proyecto_ibfk_13` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `red`
--

DROP TABLE IF EXISTS `red`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `red` (
  `idred` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_red` varchar(50) DEFAULT NULL,
  `fkidtipo_red` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idred`),
  KEY `tipo_redred` (`fkidtipo_red`),
  CONSTRAINT `red_ibfk_1` FOREIGN KEY (`fkidtipo_red`) REFERENCES `tipo_red` (`idtipo_red`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `red`
--

LOCK TABLES `red` WRITE;
/*!40000 ALTER TABLE `red` DISABLE KEYS */;
/*!40000 ALTER TABLE `red` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_cable`
--

DROP TABLE IF EXISTS `tipo_cable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_cable` (
  `idtipo_cable` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_cable` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_cable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_cable`
--

LOCK TABLES `tipo_cable` WRITE;
/*!40000 ALTER TABLE `tipo_cable` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_cable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_dato`
--

DROP TABLE IF EXISTS `tipo_dato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_dato` (
  `idtipo_dato` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_dato` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_dato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_dato`
--

LOCK TABLES `tipo_dato` WRITE;
/*!40000 ALTER TABLE `tipo_dato` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_dato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_empresa_cliente`
--

DROP TABLE IF EXISTS `tipo_empresa_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_empresa_cliente` (
  `idtipo_empresa_cliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_empresa_cliente` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_empresa_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_empresa_cliente`
--

LOCK TABLES `tipo_empresa_cliente` WRITE;
/*!40000 ALTER TABLE `tipo_empresa_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_empresa_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_materia_prima`
--

DROP TABLE IF EXISTS `tipo_materia_prima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_materia_prima` (
  `idtipo_materia_prima` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_materia_prima` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_materia_prima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_materia_prima`
--

LOCK TABLES `tipo_materia_prima` WRITE;
/*!40000 ALTER TABLE `tipo_materia_prima` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_materia_prima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_protocolo`
--

DROP TABLE IF EXISTS `tipo_protocolo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_protocolo` (
  `idtipo_protocolo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_protocolo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_protocolo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_protocolo`
--

LOCK TABLES `tipo_protocolo` WRITE;
/*!40000 ALTER TABLE `tipo_protocolo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_protocolo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_red`
--

DROP TABLE IF EXISTS `tipo_red`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_red` (
  `idtipo_red` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_cable` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_red`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_red`
--

LOCK TABLES `tipo_red` WRITE;
/*!40000 ALTER TABLE `tipo_red` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_red` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipologia`
--

DROP TABLE IF EXISTS `tipologia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipologia` (
  `idtipologia` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_tipologia` varchar(50) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idtipologia`),
  KEY `estadotipologia` (`fkidestado`),
  CONSTRAINT `tipologia_ibfk_1` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipologia`
--

LOCK TABLES `tipologia` WRITE;
/*!40000 ALTER TABLE `tipologia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipologia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicacion`
--

DROP TABLE IF EXISTS `ubicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ubicacion` (
  `idubicacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `ubicacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ubicacion`
--

LOCK TABLES `ubicacion` WRITE;
/*!40000 ALTER TABLE `ubicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ubicacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-19 11:24:01
