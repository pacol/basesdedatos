-- MySQL dump 10.13  Distrib 5.6.12, for Win32 (x86)
--
-- Host: localhost    Database: lola
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acta`
--

DROP TABLE IF EXISTS `acta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acta` (
  `Id_Acta` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Estado` varchar(20) NOT NULL,
  `Fecha` datetime NOT NULL,
  `FK_Id_Alumno` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Acta`),
  KEY `ActaAlumno` (`FK_Id_Alumno`),
  CONSTRAINT `acta_ibfk_1` FOREIGN KEY (`FK_Id_Alumno`) REFERENCES `alumno` (`Id_Alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acta`
--

LOCK TABLES `acta` WRITE;
/*!40000 ALTER TABLE `acta` DISABLE KEYS */;
/*!40000 ALTER TABLE `acta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actaempleado`
--

DROP TABLE IF EXISTS `actaempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actaempleado` (
  `Id_ActaEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `FK_Id_Acta` bigint(20) NOT NULL,
  `FK_Id_Empleado` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_ActaEmpleado`),
  KEY `ActaEmpleadoActa` (`FK_Id_Acta`),
  KEY `ActaEmpleadoEmpleado` (`FK_Id_Empleado`),
  CONSTRAINT `actaempleado_ibfk_4` FOREIGN KEY (`FK_Id_Empleado`) REFERENCES `empleado` (`Id_Empleado`),
  CONSTRAINT `actaempleado_ibfk_1` FOREIGN KEY (`FK_Id_Empleado`) REFERENCES `empleado` (`Id_Empleado`),
  CONSTRAINT `actaempleado_ibfk_2` FOREIGN KEY (`FK_Id_Acta`) REFERENCES `acta` (`Id_Acta`),
  CONSTRAINT `actaempleado_ibfk_3` FOREIGN KEY (`FK_Id_Acta`) REFERENCES `acta` (`Id_Acta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actaempleado`
--

LOCK TABLES `actaempleado` WRITE;
/*!40000 ALTER TABLE `actaempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `actaempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acudiente`
--

DROP TABLE IF EXISTS `acudiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acudiente` (
  `Id_Acudiente` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Acudiente` varchar(30) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Celular` bigint(11) NOT NULL,
  `Email` varchar(20) NOT NULL,
  `FK_Id_Pais` bigint(20) NOT NULL,
  `FK_Id_Departamento` bigint(20) NOT NULL,
  `FK_Id_Ciudad` bigint(20) NOT NULL,
  `FK_Id_Localidad` bigint(20) NOT NULL,
  `FK_Id_Barrio` bigint(20) NOT NULL,
  `FK_Id_RH` bigint(20) NOT NULL,
  `FK_Id_Eps` bigint(20) NOT NULL,
  `FK_Id_estadoCivil` bigint(20) NOT NULL,
  `FK_Id_Genero` bigint(20) NOT NULL,
  `FK_Id_tipoSangre` bigint(20) NOT NULL,
  `FK_Id_tipoDocumento` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Acudiente`),
  KEY `AcudientePais` (`FK_Id_Pais`),
  KEY `AcudienteDepartamento` (`FK_Id_Departamento`),
  KEY `AcudienteCiudad` (`FK_Id_Ciudad`),
  KEY `AcudienteLocalidad` (`FK_Id_Localidad`),
  KEY `AcudienteBarrio` (`FK_Id_Barrio`),
  KEY `AcudienteRH` (`FK_Id_RH`),
  KEY `AcudienteEps` (`FK_Id_Eps`),
  KEY `AcudienteestadoCivil` (`FK_Id_estadoCivil`),
  KEY `AcudienteGenero` (`FK_Id_Genero`),
  KEY `AcudientetipoSangre` (`FK_Id_tipoSangre`),
  KEY `AcudientetipoDocumento` (`FK_Id_tipoDocumento`),
  CONSTRAINT `acudiente_ibfk_11` FOREIGN KEY (`FK_Id_tipoDocumento`) REFERENCES `tipodocumento` (`Id_tipoDocumento`),
  CONSTRAINT `acudiente_ibfk_1` FOREIGN KEY (`FK_Id_Pais`) REFERENCES `pais` (`Id_Pais`),
  CONSTRAINT `acudiente_ibfk_10` FOREIGN KEY (`FK_Id_tipoSangre`) REFERENCES `tiposangre` (`Id_tipoSangre`),
  CONSTRAINT `acudiente_ibfk_2` FOREIGN KEY (`FK_Id_Departamento`) REFERENCES `departamento` (`Id_Departamento`),
  CONSTRAINT `acudiente_ibfk_3` FOREIGN KEY (`FK_Id_Ciudad`) REFERENCES `ciudad` (`Id_ciudad`),
  CONSTRAINT `acudiente_ibfk_4` FOREIGN KEY (`FK_Id_Localidad`) REFERENCES `localidad` (`Id_Localidad`),
  CONSTRAINT `acudiente_ibfk_5` FOREIGN KEY (`FK_Id_Barrio`) REFERENCES `barrio` (`Id_Barrio`),
  CONSTRAINT `acudiente_ibfk_6` FOREIGN KEY (`FK_Id_RH`) REFERENCES `rh` (`Id_RH`),
  CONSTRAINT `acudiente_ibfk_7` FOREIGN KEY (`FK_Id_Eps`) REFERENCES `eps` (`Id_Eps`),
  CONSTRAINT `acudiente_ibfk_8` FOREIGN KEY (`FK_Id_estadoCivil`) REFERENCES `estadocivil` (`Id_EstadoCivil`),
  CONSTRAINT `acudiente_ibfk_9` FOREIGN KEY (`FK_Id_Genero`) REFERENCES `genero` (`Id_Genero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acudiente`
--

LOCK TABLES `acudiente` WRITE;
/*!40000 ALTER TABLE `acudiente` DISABLE KEYS */;
/*!40000 ALTER TABLE `acudiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `Id_Alumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Alumno` varchar(30) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Celular` bigint(11) NOT NULL,
  `Email` varchar(20) NOT NULL,
  `FK_Id_Pais` bigint(20) NOT NULL,
  `FK_Id_Departamento` bigint(20) NOT NULL,
  `FK_Id_Ciudad` bigint(20) NOT NULL,
  `FK_Id_Localidad` bigint(20) NOT NULL,
  `FK_Id_Barrio` bigint(20) NOT NULL,
  `FK_Id_RH` bigint(20) NOT NULL,
  `FK_Id_Eps` bigint(20) NOT NULL,
  `FK_Id_estadoCivil` bigint(20) NOT NULL,
  `FK_Id_Genero` bigint(20) NOT NULL,
  `FK_Id_tipoSangre` bigint(20) NOT NULL,
  `FK_Id_tipoDocumento` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Alumno`),
  KEY `AlumnoPais` (`FK_Id_Pais`),
  KEY `AlumnoDepartamento` (`FK_Id_Departamento`),
  KEY `AlumnoCiudad` (`FK_Id_Ciudad`),
  KEY `AlumnoLocalidad` (`FK_Id_Localidad`),
  KEY `AlumnoBarrio` (`FK_Id_Barrio`),
  KEY `AlumnoRH` (`FK_Id_RH`),
  KEY `AlumnoEps` (`FK_Id_Eps`),
  KEY `AlumnoestadoCivil` (`FK_Id_estadoCivil`),
  KEY `AlumnoGenero` (`FK_Id_Genero`),
  KEY `AlumnotipoSangre` (`FK_Id_tipoSangre`),
  KEY `AlumnotipoDocumento` (`FK_Id_tipoDocumento`),
  CONSTRAINT `alumno_ibfk_11` FOREIGN KEY (`FK_Id_tipoDocumento`) REFERENCES `tipodocumento` (`Id_tipoDocumento`),
  CONSTRAINT `alumno_ibfk_1` FOREIGN KEY (`FK_Id_Pais`) REFERENCES `pais` (`Id_Pais`),
  CONSTRAINT `alumno_ibfk_10` FOREIGN KEY (`FK_Id_tipoSangre`) REFERENCES `tiposangre` (`Id_tipoSangre`),
  CONSTRAINT `alumno_ibfk_2` FOREIGN KEY (`FK_Id_Departamento`) REFERENCES `departamento` (`Id_Departamento`),
  CONSTRAINT `alumno_ibfk_3` FOREIGN KEY (`FK_Id_Ciudad`) REFERENCES `ciudad` (`Id_ciudad`),
  CONSTRAINT `alumno_ibfk_4` FOREIGN KEY (`FK_Id_Localidad`) REFERENCES `localidad` (`Id_Localidad`),
  CONSTRAINT `alumno_ibfk_5` FOREIGN KEY (`FK_Id_Barrio`) REFERENCES `barrio` (`Id_Barrio`),
  CONSTRAINT `alumno_ibfk_6` FOREIGN KEY (`FK_Id_RH`) REFERENCES `rh` (`Id_RH`),
  CONSTRAINT `alumno_ibfk_7` FOREIGN KEY (`FK_Id_Eps`) REFERENCES `eps` (`Id_Eps`),
  CONSTRAINT `alumno_ibfk_8` FOREIGN KEY (`FK_Id_estadoCivil`) REFERENCES `estadocivil` (`Id_EstadoCivil`),
  CONSTRAINT `alumno_ibfk_9` FOREIGN KEY (`FK_Id_Genero`) REFERENCES `genero` (`Id_Genero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `Id_Asignatura` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Asignatura` varchar(20) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `FK_Id_Curso` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Asignatura`),
  KEY `AsignaturaCurso` (`FK_Id_Curso`),
  CONSTRAINT `asignatura_ibfk_1` FOREIGN KEY (`FK_Id_Curso`) REFERENCES `curso` (`Id_Curso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignaturaboletin`
--

DROP TABLE IF EXISTS `asignaturaboletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignaturaboletin` (
  `Id_AsignaturaBoletin` bigint(20) NOT NULL AUTO_INCREMENT,
  `FK_Id_Asignatura` bigint(20) NOT NULL,
  `FK_Id_Boletin` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_AsignaturaBoletin`),
  KEY `AsignaturaBoletinBoletin` (`FK_Id_Boletin`),
  KEY `AsignaturaBoletinAsignatura` (`FK_Id_Asignatura`),
  CONSTRAINT `asignaturaboletin_ibfk_2` FOREIGN KEY (`FK_Id_Asignatura`) REFERENCES `asignatura` (`Id_Asignatura`),
  CONSTRAINT `asignaturaboletin_ibfk_1` FOREIGN KEY (`FK_Id_Boletin`) REFERENCES `boletin` (`Id_Boletin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignaturaboletin`
--

LOCK TABLES `asignaturaboletin` WRITE;
/*!40000 ALTER TABLE `asignaturaboletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignaturaboletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barrio`
--

DROP TABLE IF EXISTS `barrio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barrio` (
  `Id_Barrio` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Barrio` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Barrio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barrio`
--

LOCK TABLES `barrio` WRITE;
/*!40000 ALTER TABLE `barrio` DISABLE KEYS */;
/*!40000 ALTER TABLE `barrio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletin`
--

DROP TABLE IF EXISTS `boletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boletin` (
  `Id_Boletin` bigint(20) NOT NULL AUTO_INCREMENT,
  `Periodo` bigint(20) NOT NULL,
  `FK_Id_Empleado` bigint(20) NOT NULL,
  `FK_Id_Alumno` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Boletin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletin`
--

LOCK TABLES `boletin` WRITE;
/*!40000 ALTER TABLE `boletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `boletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `Id_Campus` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `FK_Id_tipoCampus` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Campus`),
  KEY `CampustipoCampus` (`FK_Id_tipoCampus`),
  CONSTRAINT `campus_ibfk_1` FOREIGN KEY (`FK_Id_tipoCampus`) REFERENCES `tipocampus` (`Id_tipoCampus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `Id_ciudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Ciudad` varchar(30) NOT NULL,
  `FK_Id_Localidad` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_ciudad`),
  KEY `CiudadLocalidad` (`FK_Id_Localidad`),
  CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`FK_Id_Localidad`) REFERENCES `localidad` (`Id_Localidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convocatoria`
--

DROP TABLE IF EXISTS `convocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convocatoria` (
  `Id_Convocatoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `Especificacion` varchar(100) NOT NULL,
  `FechaConvocatoria` datetime NOT NULL,
  `Lugar` varchar(30) NOT NULL,
  `FK_Id_TipoConvocatoria` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Convocatoria`),
  KEY `ConvocatoriatipoConvocatoria` (`FK_Id_TipoConvocatoria`),
  CONSTRAINT `convocatoria_ibfk_1` FOREIGN KEY (`FK_Id_TipoConvocatoria`) REFERENCES `tipoconvocatoria` (`Id_TipoConvocatoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convocatoria`
--

LOCK TABLES `convocatoria` WRITE;
/*!40000 ALTER TABLE `convocatoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `convocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `Id_Curso` bigint(20) NOT NULL AUTO_INCREMENT,
  `Numero` bigint(20) NOT NULL,
  `FK_Id_Empleado` bigint(20) NOT NULL,
  `FK_id_Alumno` bigint(20) NOT NULL,
  `FK_Id_Grupo` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Curso`),
  KEY `CursoEmpleado` (`FK_Id_Empleado`),
  KEY `CursoAlumno` (`FK_id_Alumno`),
  KEY `CursoGrupo` (`FK_Id_Grupo`),
  CONSTRAINT `curso_ibfk_3` FOREIGN KEY (`FK_Id_Grupo`) REFERENCES `grupo` (`Id_Grupo`),
  CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`FK_Id_Empleado`) REFERENCES `empleado` (`Id_Empleado`),
  CONSTRAINT `curso_ibfk_2` FOREIGN KEY (`FK_id_Alumno`) REFERENCES `alumno` (`Id_Alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `Id_Departamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Departamento` varchar(30) NOT NULL,
  `FK_Id_Ciudad` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Departamento`),
  KEY `DepartamentoCiudad` (`FK_Id_Ciudad`),
  CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`FK_Id_Ciudad`) REFERENCES `ciudad` (`Id_ciudad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `Id_Empleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Empleado` varchar(30) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Celular` bigint(11) NOT NULL,
  `Email` varchar(20) NOT NULL,
  `FK_Id_Pais` bigint(20) NOT NULL,
  `FK_Id_Departamento` bigint(20) NOT NULL,
  `FK_Id_Ciudad` bigint(20) NOT NULL,
  `FK_Id_Localidad` bigint(20) NOT NULL,
  `FK_Id_Barrio` bigint(20) NOT NULL,
  `FK_Id_RH` bigint(20) NOT NULL,
  `FK_Id_Eps` bigint(20) NOT NULL,
  `FK_Id_estadoCivil` bigint(20) NOT NULL,
  `FK_Id_Genero` bigint(20) NOT NULL,
  `FK_Id_tipoSangre` bigint(20) NOT NULL,
  `FK_Id_tipoDocumento` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Empleado`),
  KEY `EmpleadoPais` (`FK_Id_Pais`),
  KEY `EmpleadoDepartamento` (`FK_Id_Departamento`),
  KEY `EmpleadoCiudad` (`FK_Id_Ciudad`),
  KEY `EmpleadoLocalidad` (`FK_Id_Localidad`),
  KEY `EmpleadoBarrio` (`FK_Id_Barrio`),
  KEY `EmpleadoRH` (`FK_Id_RH`),
  KEY `EmpleadoEps` (`FK_Id_Eps`),
  KEY `EmpleadoestadoCivil` (`FK_Id_estadoCivil`),
  KEY `EmpleadoGenero` (`FK_Id_Genero`),
  KEY `EmpleadotipoSangre` (`FK_Id_tipoSangre`),
  KEY `EmpleadotipoDocumento` (`FK_Id_tipoDocumento`),
  CONSTRAINT `empleado_ibfk_11` FOREIGN KEY (`FK_Id_tipoDocumento`) REFERENCES `tipodocumento` (`Id_tipoDocumento`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`FK_Id_Pais`) REFERENCES `pais` (`Id_Pais`),
  CONSTRAINT `empleado_ibfk_10` FOREIGN KEY (`FK_Id_tipoSangre`) REFERENCES `tiposangre` (`Id_tipoSangre`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`FK_Id_Departamento`) REFERENCES `departamento` (`Id_Departamento`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`FK_Id_Ciudad`) REFERENCES `ciudad` (`Id_ciudad`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`FK_Id_Localidad`) REFERENCES `localidad` (`Id_Localidad`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`FK_Id_Barrio`) REFERENCES `barrio` (`Id_Barrio`),
  CONSTRAINT `empleado_ibfk_6` FOREIGN KEY (`FK_Id_RH`) REFERENCES `rh` (`Id_RH`),
  CONSTRAINT `empleado_ibfk_7` FOREIGN KEY (`FK_Id_Eps`) REFERENCES `eps` (`Id_Eps`),
  CONSTRAINT `empleado_ibfk_8` FOREIGN KEY (`FK_Id_estadoCivil`) REFERENCES `estadocivil` (`Id_EstadoCivil`),
  CONSTRAINT `empleado_ibfk_9` FOREIGN KEY (`FK_Id_Genero`) REFERENCES `genero` (`Id_Genero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleadorol`
--

DROP TABLE IF EXISTS `empleadorol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleadorol` (
  `Id_Detalle_EmpleadoRol` bigint(20) NOT NULL AUTO_INCREMENT,
  `FK_Empleado` bigint(20) NOT NULL,
  `FK_Rol` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Detalle_EmpleadoRol`),
  KEY `EmpleadoRolRol` (`FK_Rol`),
  CONSTRAINT `empleadorol_ibfk_1` FOREIGN KEY (`FK_Rol`) REFERENCES `rol` (`Id_Rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleadorol`
--

LOCK TABLES `empleadorol` WRITE;
/*!40000 ALTER TABLE `empleadorol` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleadorol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `Id_Eps` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Eps` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Eps`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadocivil`
--

DROP TABLE IF EXISTS `estadocivil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadocivil` (
  `Id_EstadoCivil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_EstadoCivil` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_EstadoCivil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadocivil`
--

LOCK TABLES `estadocivil` WRITE;
/*!40000 ALTER TABLE `estadocivil` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadocivil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoformacion`
--

DROP TABLE IF EXISTS `estadoformacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoformacion` (
  `Id_estadoFormacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_estadoFormacion` varchar(50) NOT NULL,
  PRIMARY KEY (`Id_estadoFormacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoformacion`
--

LOCK TABLES `estadoformacion` WRITE;
/*!40000 ALTER TABLE `estadoformacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoformacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadomatricula`
--

DROP TABLE IF EXISTS `estadomatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadomatricula` (
  `Id_estadoMatricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_estadoMatricula` varchar(50) NOT NULL,
  `FK_Id_estadoFormacion` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_estadoMatricula`),
  KEY `estadoMatricula_estadoFormacion` (`FK_Id_estadoFormacion`),
  CONSTRAINT `estadomatricula_ibfk_1` FOREIGN KEY (`FK_Id_estadoFormacion`) REFERENCES `estadoformacion` (`Id_estadoFormacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadomatricula`
--

LOCK TABLES `estadomatricula` WRITE;
/*!40000 ALTER TABLE `estadomatricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadomatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etnia`
--

DROP TABLE IF EXISTS `etnia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etnia` (
  `Id_Etnia` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Etnia` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Etnia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etnia`
--

LOCK TABLES `etnia` WRITE;
/*!40000 ALTER TABLE `etnia` DISABLE KEYS */;
/*!40000 ALTER TABLE `etnia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `Id_Genero` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Genero` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Genero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `Id_Grupo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tema` varchar(50) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  PRIMARY KEY (`Id_Grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institucion`
--

DROP TABLE IF EXISTS `institucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institucion` (
  `Id_Institucion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Institucion` varchar(40) NOT NULL,
  `Direccion` varchar(30) NOT NULL,
  `FK_Id_Convocatoria` bigint(20) NOT NULL,
  `FK_Id_Empleado` bigint(20) NOT NULL,
  `FK_id_Matricula` bigint(20) NOT NULL,
  `FK_Id_Jornada` bigint(20) NOT NULL,
  `FK_Id_Barrio` bigint(20) NOT NULL,
  `Fk_Id_Titulacion` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Institucion`),
  KEY `InstitucionConvocatoria` (`FK_Id_Convocatoria`),
  KEY `InstitucionEmpleado` (`FK_Id_Empleado`),
  KEY `InstitucionMatricula` (`FK_id_Matricula`),
  KEY `InstitucionJornada` (`FK_Id_Jornada`),
  KEY `InstitucionBarrio` (`FK_Id_Barrio`),
  KEY `InstitucionTitulacion` (`Fk_Id_Titulacion`),
  CONSTRAINT `institucion_ibfk_6` FOREIGN KEY (`Fk_Id_Titulacion`) REFERENCES `titulacion` (`Id_Titulacion`),
  CONSTRAINT `institucion_ibfk_1` FOREIGN KEY (`FK_Id_Convocatoria`) REFERENCES `convocatoria` (`Id_Convocatoria`),
  CONSTRAINT `institucion_ibfk_2` FOREIGN KEY (`FK_Id_Empleado`) REFERENCES `empleado` (`Id_Empleado`),
  CONSTRAINT `institucion_ibfk_3` FOREIGN KEY (`FK_id_Matricula`) REFERENCES `matricula` (`Id_Matricula`),
  CONSTRAINT `institucion_ibfk_4` FOREIGN KEY (`FK_Id_Jornada`) REFERENCES `jornada` (`Id_Jornada`),
  CONSTRAINT `institucion_ibfk_5` FOREIGN KEY (`FK_Id_Barrio`) REFERENCES `barrio` (`Id_Barrio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institucion`
--

LOCK TABLES `institucion` WRITE;
/*!40000 ALTER TABLE `institucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `institucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jornada`
--

DROP TABLE IF EXISTS `jornada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jornada` (
  `Id_Jornada` bigint(20) NOT NULL AUTO_INCREMENT,
  `turno` varchar(30) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`Id_Jornada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jornada`
--

LOCK TABLES `jornada` WRITE;
/*!40000 ALTER TABLE `jornada` DISABLE KEYS */;
/*!40000 ALTER TABLE `jornada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `Id_Localidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Localidad` varchar(30) NOT NULL,
  `FK_Id_Barrio` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Localidad`),
  KEY `LocalidadBarrio` (`FK_Id_Barrio`),
  CONSTRAINT `localidad_ibfk_1` FOREIGN KEY (`FK_Id_Barrio`) REFERENCES `barrio` (`Id_Barrio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `Id_Matricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `Costo` bigint(20) NOT NULL,
  `FK_Id_tipoPoblacion` bigint(20) NOT NULL,
  `FK_Id_Acudiente` bigint(20) NOT NULL,
  `FK_Id_Alumno` bigint(20) NOT NULL,
  `FK_Id_estadoMatricula` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Matricula`),
  KEY `MatriculaAcudiente` (`FK_Id_Acudiente`),
  KEY `MatriculaestadoMatricula` (`FK_Id_estadoMatricula`),
  KEY `MatriculaAlumno` (`FK_Id_Alumno`),
  KEY `MatriculatipoPoblacion` (`FK_Id_tipoPoblacion`),
  CONSTRAINT `matricula_ibfk_4` FOREIGN KEY (`FK_Id_tipoPoblacion`) REFERENCES `tipopoblacion` (`Id_tipoPoblacion`),
  CONSTRAINT `matricula_ibfk_1` FOREIGN KEY (`FK_Id_Acudiente`) REFERENCES `acudiente` (`Id_Acudiente`),
  CONSTRAINT `matricula_ibfk_2` FOREIGN KEY (`FK_Id_estadoMatricula`) REFERENCES `estadomatricula` (`Id_estadoMatricula`),
  CONSTRAINT `matricula_ibfk_3` FOREIGN KEY (`FK_Id_Alumno`) REFERENCES `alumno` (`Id_Alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matricula`
--

LOCK TABLES `matricula` WRITE;
/*!40000 ALTER TABLE `matricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `matricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matriculaalumno`
--

DROP TABLE IF EXISTS `matriculaalumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matriculaalumno` (
  `id_MatriculaAlumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `FK_Matricula` bigint(20) NOT NULL,
  `FK_Alumno` bigint(20) NOT NULL,
  PRIMARY KEY (`id_MatriculaAlumno`),
  KEY `MatriculaAlumnoMatricula` (`FK_Matricula`),
  KEY `MatriculaAlumnoAlumno` (`FK_Alumno`),
  CONSTRAINT `matriculaalumno_ibfk_2` FOREIGN KEY (`FK_Alumno`) REFERENCES `alumno` (`Id_Alumno`),
  CONSTRAINT `matriculaalumno_ibfk_1` FOREIGN KEY (`FK_Matricula`) REFERENCES `matricula` (`Id_Matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matriculaalumno`
--

LOCK TABLES `matriculaalumno` WRITE;
/*!40000 ALTER TABLE `matriculaalumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `matriculaalumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `Id_Pais` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Pais` varchar(30) NOT NULL,
  `FK_Id_Departamento` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Pais`),
  KEY `PaisDepartamento` (`FK_Id_Departamento`),
  CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`FK_Id_Departamento`) REFERENCES `departamento` (`Id_Departamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rh`
--

DROP TABLE IF EXISTS `rh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rh` (
  `Id_RH` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_RH` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_RH`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rh`
--

LOCK TABLES `rh` WRITE;
/*!40000 ALTER TABLE `rh` DISABLE KEYS */;
/*!40000 ALTER TABLE `rh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `Id_Rol` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Rol` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_Rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocampus`
--

DROP TABLE IF EXISTS `tipocampus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocampus` (
  `Id_tipoCampus` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_tipoCampus` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_tipoCampus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocampus`
--

LOCK TABLES `tipocampus` WRITE;
/*!40000 ALTER TABLE `tipocampus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipocampus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoconvocatoria`
--

DROP TABLE IF EXISTS `tipoconvocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoconvocatoria` (
  `Id_TipoConvocatoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_tipoConvocatoria` varchar(40) NOT NULL,
  `FK_Id_Campus` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_TipoConvocatoria`),
  KEY `tipoConvocatoriaCampus` (`FK_Id_Campus`),
  CONSTRAINT `tipoconvocatoria_ibfk_1` FOREIGN KEY (`FK_Id_Campus`) REFERENCES `campus` (`Id_Campus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoconvocatoria`
--

LOCK TABLES `tipoconvocatoria` WRITE;
/*!40000 ALTER TABLE `tipoconvocatoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoconvocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `Id_tipoDocumento` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_tipoDocumento` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_tipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopoblacion`
--

DROP TABLE IF EXISTS `tipopoblacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopoblacion` (
  `Id_tipoPoblacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `FK_Id_Etnia` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_tipoPoblacion`),
  KEY `tipoPoblacionEtnia` (`FK_Id_Etnia`),
  CONSTRAINT `tipopoblacion_ibfk_1` FOREIGN KEY (`FK_Id_Etnia`) REFERENCES `etnia` (`Id_Etnia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopoblacion`
--

LOCK TABLES `tipopoblacion` WRITE;
/*!40000 ALTER TABLE `tipopoblacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipopoblacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposangre`
--

DROP TABLE IF EXISTS `tiposangre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposangre` (
  `Id_tipoSangre` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_tipoSangre` varchar(30) NOT NULL,
  PRIMARY KEY (`Id_tipoSangre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposangre`
--

LOCK TABLES `tiposangre` WRITE;
/*!40000 ALTER TABLE `tiposangre` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiposangre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titulacion`
--

DROP TABLE IF EXISTS `titulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titulacion` (
  `Id_Titulacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre_Titulacion` varchar(30) NOT NULL,
  `cargaElectiva` varchar(50) NOT NULL,
  `totalCreditos` bigint(20) NOT NULL,
  `FK_Id_Campus` bigint(20) NOT NULL,
  PRIMARY KEY (`Id_Titulacion`),
  KEY `TitulacionCampus` (`FK_Id_Campus`),
  CONSTRAINT `titulacion_ibfk_1` FOREIGN KEY (`FK_Id_Campus`) REFERENCES `campus` (`Id_Campus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titulacion`
--

LOCK TABLES `titulacion` WRITE;
/*!40000 ALTER TABLE `titulacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `titulacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-19 10:07:53
